import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavInformationComponent } from './sidenav-information.component';

describe('SidenavInformationComponent', () => {
  let component: SidenavInformationComponent;
  let fixture: ComponentFixture<SidenavInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
