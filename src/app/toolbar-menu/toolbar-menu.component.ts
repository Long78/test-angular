import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toolbar-menu',
  templateUrl: './toolbar-menu.component.html',
  styleUrls: ['./toolbar-menu.component.scss']
})
export class ToolbarMenuComponent implements OnInit {

  @Output() onOff = new EventEmitter<boolean>();
  state = false;
  constructor() {
  }
  
  ngOnInit() {
  }

  buttonMenu(): void{
    this.onOff.emit(!this.state);
  } 

}
